# zolf3d

![demo](https://framagit.org/ouzb64ty/zolf3d/-/raw/main/img/demo.png?ref_type=heads)

Challenge de développement d'un Wolf 3D personnel sous Linux en C avec la SDL en 1 semaine. (non fignolé) ✅

## Installation

Compilation:
```bash
make
```

Usage souhaité:
```bash
./zolf3d <map>
```

## Cartes

Un exemple de cartes est disponible dans le dossier _maps/_.
Les éléments d'une carte sont:
- X représente un mur
- . représente un vide
- P représente la position initiale du joueur
