#ifndef MINI_MAP_H
# define MINI_MAP_H

# include <SDL2/SDL_image.h>
# include <math.h>
# include <sys/param.h>

# include "def.h"
# include "map.h"

int
sdl_core(t_map *map);

#endif
