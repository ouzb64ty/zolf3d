#include "main.h"

int
main(int argc, char *argv[]) {

  t_map *map = NULL;
  int ret = 0;

  if (argc == 2) {
    map = map_read(argv[1]);
    if (map == NULL) {
      printf("%s", MSG_FILE_ERROR);
      return 1;
    }
    map_debug(map);
    ret = sdl_core(map);
    if (ret == -1)
      return 1;
    map_free(map);
  } else {
    printf("%s", MSG_USAGE);
  }
  return 0;
}
