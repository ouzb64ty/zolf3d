#ifndef MAP_H
# define MAP_H

# include <stdio.h>
# include <stdlib.h>
# include <string.h>

# include "def.h"

typedef struct s_map {
  int width;
  int height;
  int **map;
} t_map;

void
map_debug(t_map *map);

void
map_free(t_map *map);

t_map *
map_read(char *filename);

#endif
