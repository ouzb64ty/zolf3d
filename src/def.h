#ifndef DEF_H
# define DEF_H

# include <SDL2/SDL.h>

typedef enum e_bool {
  TRUE,
  FALSE
} t_bool;

# define MSG_USAGE "usage: ./zolf3d <map>\n"
# define MSG_FILE_ERROR "erreur: zolf3d: le fichier n'existe pas ou le format de la map est incorrect.\n"

/* Configuration */

# define APP_DELAY 16
# define FOV 90
# define MAX_DEPTH 500
# define TILE_SIZE 30
# define STEP_SIZE 3

# define MAP_MAX_WIDTH 250

# define SCREEN_WIDTH 400
# define SCREEN_HEIGHT 400

# define WALL_WIDTH 10
# define WALL_HEIGHT 10

# define ELEM_WALL 0
# define ELEM_SPACE 1
# define ELEM_PLAYER 2

typedef struct s_wall {
  int x;
  int y;
} t_wall;

# define PLAYER_WIDTH 10
# define PLAYER_HEIGHT 10

typedef struct s_player {
  t_bool init;
  double x;
  double y;
  double angle;
} t_player;

typedef struct s_app {
  SDL_Renderer *renderer;
  SDL_Window *window;
} t_app;

#endif
