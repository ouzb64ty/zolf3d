#include "map.h"

static short g_player = 0;

static void
map_convert_line_to_elem(int *map_y, char *line) {

  int i = 0;

  for (i = 0; i < (int)strlen(line) - 1; i++) {
    if (line[i] == 'X')
      map_y[i] = ELEM_WALL;
    else if (line[i] == '.')
      map_y[i] = ELEM_SPACE;
    else if (line[i] == 'P')
      map_y[i] = ELEM_PLAYER;
  }
}

static int
map_check_is_elem(char c) {

  if (c == 'X'
      || c == '.'
      || c == 'P')
    return 1;
  return 0;
}

static int
map_check_line(char line[]) {

  int i = 0;

  for (i = 0; i < (int)strlen(line) - 2; i++) {
    /* Check */
    if (map_check_is_elem(line[i]) == 0)
      return (-1);
    /* Rules for P element */
    if (line[i] == 'P' && g_player == 0)
      g_player = 1;
    else if (line[i] == 'P' && g_player == 1)
      return -1;
  }
  return 1;
}

static t_map *
map_read_init(FILE *file) {

  t_map *ret = (t_map *)malloc(sizeof(t_map));
  char line[MAP_MAX_WIDTH] = {0};
  int check = 0;

  if (file == NULL || ret == NULL) {
    free(ret);
    ret = NULL;
    return NULL;
  }
  ret->width = 0;
  ret->height = 0;
  ret->map = NULL;
  while (fgets(line, sizeof(line), file) != NULL) {
    /* Check elements */
    check = map_check_line(line);
    if (check == -1) {
      free(ret);
      ret = NULL;
      return NULL;
    }
    ret->height += 1;
    /* Map Square checking */
    if (ret->width != 0 && ret->width != (int)strlen(line) - 1) {
      free(ret);
      ret = NULL;
      return NULL;
    }
    ret->width = strlen(line) - 1;
    bzero(line, MAP_MAX_WIDTH);
  }
  /* Check minimum size & Only one Player */
  if (ret->width == 0 || ret->height == 0 || g_player == 0) {
    free(ret);
    ret = NULL;
    return NULL;
  }
  return ret;
}

void
map_free(t_map *map) {

  int y = 0;

  if (map == NULL)
    return ;
  for (y = 0; y < map->height; y++) {
    free(map->map[y]);
  }
  if (map != NULL) {
    free(map->map);
    map->map = NULL;
  }
  free(map);
  map = NULL;
}

void
map_debug(t_map *map) {

  int y = 0;
  int x = 0;

  if (map == NULL || map->map == NULL) {
    printf("La map est NULL\n");
    return ;
  }
  printf("Longueur de la map : %d\n", map->width);
  printf("Largeur de la map : %d\n", map->height);
  for (y = 0; y < map->height; y++) {
    for (x = 0; x < map->width; x++) {
      printf("%d", map->map[y][x]);
    }
    printf("\n");
  }
}

t_map *
map_read(char *filename) {

  FILE *file = fopen(filename, "r");
  char line[MAP_MAX_WIDTH] = {0};
  t_map *ret = map_read_init(file);
  int y = 0;

  if (ret == NULL) {
    //fclose(file);
    return NULL;
  }
  ret->map = (int **)malloc(sizeof(int *) * ret->height);
  if (ret->map == NULL) {
    fclose(file);
    return NULL;
  }
  rewind(file);
  while (fgets(line, sizeof(line), file) != NULL) {
    ret->map[y] = (int *)malloc(sizeof(int) * ret->width);
    map_convert_line_to_elem(ret->map[y], line);
    y += 1;
  }
  fclose(file);
  return ret;
}
