#include "mini_map.h"

static void
sdl_init(t_app *app, t_map *map) {

  int rendererFlags = SDL_RENDERER_ACCELERATED;

  if (SDL_Init(SDL_INIT_VIDEO) < 0)
    return ;
  IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG);
  app->window = SDL_CreateWindow(
      "Zolf3D",
      SDL_WINDOWPOS_CENTERED,
      SDL_WINDOWPOS_CENTERED,
      SCREEN_WIDTH,
      SCREEN_HEIGHT,
      SDL_WINDOW_SHOWN);
  if (app->window == NULL) {
    return ;
  }
  app->renderer = SDL_CreateRenderer(app->window, -1, rendererFlags);
  if (app->renderer == NULL) {
    return ;
  }
}

static void
sdl_event(t_app *app, t_player *player, t_map *map) {

  SDL_Event event;
  double tmp_player_x = 0;
  double tmp_player_y = 0;
  int tmp_floor_player_x = 0;
  int tmp_floor_player_y = 0;
  int floor_player_x = 0;
  int floor_player_y = 0;

  (void)app;
  while (SDL_PollEvent(&event)) {
    if (event.type == SDL_QUIT) {
      exit(0);
    } else if (event.type == SDL_KEYDOWN) {
      switch (event.key.keysym.sym) {
        case SDLK_LEFT:
          player->angle -= STEP_SIZE;
          break ;
        case SDLK_RIGHT:
          player->angle += STEP_SIZE;
          break ;
        case SDLK_UP:

          /* Calculate next position */
          tmp_player_x = player->x + (STEP_SIZE * cos((player->angle) * M_PI / 180)) * 1;
          tmp_player_y = player->y + (STEP_SIZE * sin((player->angle) * M_PI / 180)) * 1;

          /* Collision */
          tmp_floor_player_x = (int)floor(tmp_player_x + (TILE_SIZE / 2)) / TILE_SIZE;
          tmp_floor_player_y = (int)floor(tmp_player_y + (TILE_SIZE / 2)) / TILE_SIZE;
          floor_player_x = (int)floor(player->x + (TILE_SIZE / 2)) / TILE_SIZE;
          floor_player_y = (int)floor(player->y + (TILE_SIZE / 2)) / TILE_SIZE;
          printf("%d %d\n", tmp_floor_player_y, tmp_floor_player_x);
          if (map->map[tmp_floor_player_y][tmp_floor_player_x] == ELEM_SPACE
              || map->map[tmp_floor_player_y][tmp_floor_player_x] == ELEM_PLAYER) {
            player->x = tmp_player_x;
            player->y = tmp_player_y;
          } else if (map->map[floor_player_y][tmp_floor_player_x] == ELEM_SPACE
              || map->map[floor_player_y][tmp_floor_player_x] == ELEM_PLAYER) {
            player->x = tmp_player_x;
          } else if (map->map[tmp_floor_player_y][floor_player_x] == ELEM_SPACE
              || map->map[tmp_floor_player_y][floor_player_x] == ELEM_PLAYER) {
            player->y = tmp_player_y;
          }

          break ;
        case SDLK_DOWN:

          /* Calculate next position */
          tmp_player_x = player->x + (STEP_SIZE * cos((player->angle) * M_PI / 180)) * -1;
          tmp_player_y = player->y + (STEP_SIZE * sin((player->angle) * M_PI / 180)) * -1;

          /* Collision */
          tmp_floor_player_x = (int)floor(tmp_player_x + (TILE_SIZE / 2)) / TILE_SIZE;
          tmp_floor_player_y = (int)floor(tmp_player_y + (TILE_SIZE / 2)) / TILE_SIZE;
          floor_player_x = (int)floor(player->x + (TILE_SIZE / 2)) / TILE_SIZE;
          floor_player_y = (int)floor(player->y + (TILE_SIZE / 2)) / TILE_SIZE;
          printf("%d %d\n", tmp_floor_player_y, tmp_floor_player_x);
          if (map->map[tmp_floor_player_y][tmp_floor_player_x] == ELEM_SPACE
              || map->map[tmp_floor_player_y][tmp_floor_player_x] == ELEM_PLAYER) {
            player->x = tmp_player_x;
            player->y = tmp_player_y;
          } else if (map->map[floor_player_y][tmp_floor_player_x] == ELEM_SPACE
              || map->map[floor_player_y][tmp_floor_player_x] == ELEM_PLAYER) {
            player->y = tmp_player_y;
          } else if (map->map[tmp_floor_player_y][floor_player_x] == ELEM_SPACE
              || map->map[tmp_floor_player_y][floor_player_x] == ELEM_PLAYER) {
            player->x = tmp_player_x;
          }

          break ;
      }
    }
  }
}

static void
sdl_render(t_app *app) {

  SDL_RenderPresent(app->renderer);
}

static void
sdl_render_map(t_app *app, t_map *map, t_player *player) {

  SDL_Rect rect;
  int x = 0;
  int y = 0;
  int x_pos = 0;
  int y_pos = 0;

  for (y = 0; y / TILE_SIZE < map->height;) {
    for (x = 0; x / TILE_SIZE < map->width;) {
      rect.x = x;
      rect.y = y;
      rect.w = TILE_SIZE;
      rect.h = TILE_SIZE;
      /*if (map->map[y_pos][x_pos] == ELEM_WALL) {
        SDL_SetRenderDrawColor(app->renderer, 39, 174, 96, 255);
        continue;
      }
      else if (map->map[y_pos][x_pos] == ELEM_SPACE) {
        SDL_SetRenderDrawColor(app->renderer, 189, 195, 199, 255);
        continue;
      }*/
      if (player->init == FALSE && map->map[y_pos][x_pos] == ELEM_PLAYER) {

        /* Initialize player position */
        player->x = rect.x;
        player->y = rect.y;
        player->init = TRUE;

      }
      //SDL_RenderFillRect(app->renderer, &rect);
      x += TILE_SIZE;
      x_pos++;
    }
    y += TILE_SIZE;
    y_pos++;
    x_pos = 0;
  }
}

static void
sdl_prepare(t_app *app, t_player *player, t_map *map, SDL_Texture *texture) {

  SDL_Rect texture_dest;
  SDL_Rect sky_rect;
  SDL_Rect ground_rect;
  SDL_Rect tmp_wall_rect;;

  /* Background 2.5D Render */
  sky_rect.x = 0;
  sky_rect.y = 0;
  sky_rect.w = SCREEN_WIDTH;
  sky_rect.h = SCREEN_HEIGHT / 2;

  ground_rect.x = 0;
  ground_rect.y = SCREEN_HEIGHT / 2;
  ground_rect.w = SCREEN_WIDTH;
  ground_rect.h = SCREEN_HEIGHT / 2;

  SDL_RenderClear(app->renderer);
  SDL_SetRenderDrawColor(app->renderer, 75, 207, 250, 255);
  SDL_RenderFillRect(app->renderer, &sky_rect);
  SDL_SetRenderDrawColor(app->renderer, 5, 196, 107, 255);
  SDL_RenderFillRect(app->renderer, &ground_rect);

  /* Raycast */
  int ray = 0;
  int ray_tile = 0;
  int x = 0;
  int y = 0;
  int floor_x = 0;
  int floor_y = 0;
  int height_wall = 0;
  int brightness = 0;

  SDL_SetRenderDrawColor(app->renderer, 255, 0, 0, 255);
  for (ray = 0; ray < FOV; ray++) {
    for (ray_tile = 0; ray_tile < MAX_DEPTH; ray_tile++) {
      x = player->x + (TILE_SIZE / 2) + (ray_tile * cos((player->angle - (FOV / 2) + ray) * M_PI / 180));
      y = player->y + (TILE_SIZE / 2) + (ray_tile * sin((player->angle - (FOV / 2) + ray) * M_PI / 180));
      floor_x = (int)floor(x) / TILE_SIZE;
      floor_y = (int)floor(y) / TILE_SIZE;
      if (map->map[floor_y][floor_x] == ELEM_WALL) {

        if (ray_tile > 255)
          brightness = 255;
        else
          brightness = ray_tile;
        SDL_SetRenderDrawColor(app->renderer, 255 - brightness, 255 - brightness, 255 - brightness, 255);

        /* fix fish eye effect */
        //ray_tile *= cos((player->angle  * M_PI / 180) - (FOV / 2 - ray));

        /* Show raycast in 2.5D render */
        height_wall = 11000 / (ray_tile + 0.0001);
        //tmp_wall_rect.x = (ray / (double)FOV) * SCREEN_WIDTH;
        tmp_wall_rect.x = floor((ray / (double)FOV) * SCREEN_WIDTH);
        //tmp_wall_rect.y = (SCREEN_HEIGHT / 2) - (height_wall / 2);
        tmp_wall_rect.y = floor((SCREEN_HEIGHT / 2) - (height_wall / 2));
        tmp_wall_rect.w = (1.0 / (double)FOV) * SCREEN_WIDTH + 1;
        tmp_wall_rect.h = height_wall;

        SDL_RenderFillRect(app->renderer, &tmp_wall_rect);

        break;
      }
    }

    /* Show raycast in mini_map */
    /*SDL_RenderDrawLine(
      app->renderer,
      player->x + (TILE_SIZE / 2),
      player->y + (TILE_SIZE / 2),
      player->x + (TILE_SIZE / 2) + (ray_tile) * cos((player->angle - (FOV / 2) + ray) * M_PI / 180),
      player->y + (TILE_SIZE / 2) + (ray_tile) * sin((player->angle - (FOV / 2) + ray) * M_PI / 180));*/
  }

  /* Background */
  //SDL_SetRenderDrawColor(app->renderer, 0, 0, 0, 255);
  sdl_render_map(app, map, player);

  /* Player */
  texture_dest.x = player->x;
  texture_dest.y = player->y;
  texture_dest.w = TILE_SIZE;
  texture_dest.h = TILE_SIZE;
  //SDL_RenderCopyEx(app->renderer, texture, NULL, &texture_dest, 90 + player->angle, NULL, SDL_FLIP_NONE);

}

int
sdl_core(t_map *map) {

  t_app app;
  t_player player;
  SDL_Texture *texture = NULL;

  memset(&app, 0, sizeof(t_app));
  memset(&player, 0, sizeof(t_player));
  player.x = 0;
  player.y = 0;
  player.angle = 0;
  player.init = FALSE;
  sdl_init(&app, map);
  if (app.window == NULL || app.renderer == NULL)
    return -1;
  texture = IMG_LoadTexture(app.renderer, "img/player.png");
  while (1) {
    sdl_prepare(&app, &player, map, texture);
    sdl_event(&app, &player, map);
    sdl_render(&app);
    SDL_Delay(APP_DELAY);
  }
  SDL_DestroyTexture(texture);
  return 1;
}
